# README #

This is a simple demo application built using Spring Boot

Try the application at https://lit-crag-45970.herokuapp.com/scheduler

Running the demo locally would require Java8 and gradle.

1. Clone the repository
1. Run the application from a command prompt: gradle clean bootRun