package com.pizzacabin.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pizzacabin.model.Schedule;
import org.junit.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;

public class CustomDateDeserializerTest {

    @Test
    public void whenDeserializingDateUsingCustomDeserializer_thenCorrect() throws IOException {
        String json = "{\"ContractTimeMinutes\":100,\"Date\":\"/Date(1450051200000+0000)/\"}";
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        ObjectMapper mapper = new ObjectMapper();
        Schedule event = mapper.readerFor(Schedule.class).readValue(json);
        assertEquals("20-12-2014 02:30:00", df.format(event.Date));
    }

}