package com.pizzacabin;


import com.pizzacabin.model.*;
import org.junit.Before;
import org.junit.Test;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;

public class SchedulingServiceTest {

    private ScheduleResponse scheduleResponse;
    private SchedulingService schedulingService;
    private RestApiClient restApiClient;
    private ScheduleFormData scheduleFormData;

    @Before
    public void setup() {
        scheduleResponse = new ScheduleResponse();
        ScheduleResult scheduleResult = new ScheduleResult();
        Schedule schedule1 = scheduleBuilder(1450051200000l);
        Schedule schedule2 = scheduleBuilder(1450051200000l);
        scheduleResult.Schedules = new ArrayList<>();
        scheduleResult.Schedules.add(schedule1);
        scheduleResult.Schedules.add(schedule2);

        scheduleResponse.ScheduleResult = scheduleResult;

        restApiClient = mock(RestApiClient.class);

        scheduleFormData = new ScheduleFormData();
        scheduleFormData.setMandatoryNo(2);

        schedulingService = new SchedulingService(restApiClient);
    }

    @Test
    public void shouldReturnToTimeSlotsWhenSearchingForMinimumTwo() throws URISyntaxException {
        given(restApiClient.fetch(anyString(), any())).willReturn(scheduleResponse);

        final List<AvailableSlot> availableSlots = schedulingService.availableSlots(SchedulerController.SCHEDULE_DATE, scheduleFormData);

        assertEquals(4, availableSlots.size());

    }


    public Schedule scheduleBuilder(long date) {
        Schedule schedule = new Schedule();
        schedule.ContractTimeMinutes = 480;
        schedule.Date = new Date(date);
        schedule.Projection = projectionBuilder();
        return schedule;
    }

    public List<Projection> projectionBuilder() {
        List<Projection> list = new ArrayList();

        Projection projection = new Projection();
        projection.Start = new Date(1450080000000l);
        projection.minutes = 30;
        list.add(projection);

        Projection projection1 = new Projection();
        projection1.Start = new Date(1450087200000l);
        projection1.minutes = 30;
        list.add(projection1);

        return list;
    }
}