package com.pizzacabin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;

@Service
public class RestApiClient {

    private final RestTemplate template;


    @Autowired
    public RestApiClient(RestTemplate restTemplate) {
        this.template = restTemplate;
    }

    public <T> T fetch(String url, Class<T> responseClass) throws URISyntaxException {
        try {
            return template.getForObject(url, responseClass);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw new RuntimeException("Fallback failed for url " + url);
        }
    }
}

