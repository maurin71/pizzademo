package com.pizzacabin.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.pizzacabin.configuration.CustomDateDeserializer;

import java.util.Date;
import java.util.List;

public class Schedule {
    public int ContractTimeMinutes;

    @JsonDeserialize(using = CustomDateDeserializer.class)
    public Date Date;
    public String Name;
    public boolean IsFullDayAbsence;
    public List<Projection> Projection;


}
