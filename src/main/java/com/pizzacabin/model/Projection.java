package com.pizzacabin.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.pizzacabin.configuration.CustomDateDeserializer;

import java.util.Date;

public class Projection {
    public String name;
    public String Color;
    public String Description;
    @JsonDeserialize(using = CustomDateDeserializer.class)
    public Date Start;
    public int minutes;
}
