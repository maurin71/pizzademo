package com.pizzacabin.model;

public class ScheduleFormData {
    public int getMandatoryNo() {
        return mandatoryNo;
    }

    public void setMandatoryNo(int mandatoryNo) {
        this.mandatoryNo = mandatoryNo;
    }

    public int mandatoryNo;
    public ScheduleResponse scheduleResponse;

    public ScheduleResponse getScheduleResponse() {
        return scheduleResponse;
    }

    public void setScheduleResponse(ScheduleResponse scheduleResponse) {
        this.scheduleResponse = scheduleResponse;
    }
}
