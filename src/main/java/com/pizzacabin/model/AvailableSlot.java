package com.pizzacabin.model;

import java.util.List;

public class AvailableSlot {
    public String slotDescription;
    public String startTime;
    public String endTime;
    public int available;
    public List<Projection> projections;

}
