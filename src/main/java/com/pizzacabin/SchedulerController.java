package com.pizzacabin;

import com.pizzacabin.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.net.URISyntaxException;

@Controller
public class SchedulerController {

    // Not implemented for multiple or other dates
    final public static String SCHEDULE_DATE = "2015-12-14";

    @Autowired
    SchedulingService schedulingService;

    @GetMapping("/scheduler")
    public String getForm(Model model) {
        model.addAttribute("scheduler", new ScheduleFormData());
        return "scheduler";
    }

    @PostMapping("/scheduler")
    public String schedulerAction(final ScheduleFormData scheduler, final BindingResult bindingResult, final ModelMap model) throws URISyntaxException {
        model.addAttribute("slots", schedulingService.availableSlots(SCHEDULE_DATE, scheduler));
        model.addAttribute("scheduler", scheduler);
        return "scheduler";
    }
}
