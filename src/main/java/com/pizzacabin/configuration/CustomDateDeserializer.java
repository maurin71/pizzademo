package com.pizzacabin.configuration;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomDateDeserializer extends StdDeserializer<Date> {
    final Pattern pattern
            = Pattern.compile("/Date\\((\\d+)([-+]\\d+)\\)/");

    public CustomDateDeserializer() {
        this(null);
    }

    public CustomDateDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Date deserialize(JsonParser jsonparser, DeserializationContext context) throws IOException {
        String date = jsonparser.getValueAsString();

        System.out.println(date);
        final Matcher matcher = pattern.matcher(date);

        if (!matcher.matches()) {

            System.err.println("Bad pattern!"); // Yuck
            return new Date();
        }

        final long millis = Long.parseLong(matcher.group(1));
        String tz = matcher.group(2);
        if (tz.isEmpty())
            tz = "+0000";

        return new Date(millis);
    }
}