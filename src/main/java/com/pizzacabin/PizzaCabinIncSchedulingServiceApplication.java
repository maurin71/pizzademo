package com.pizzacabin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class PizzaCabinIncSchedulingServiceApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PizzaCabinIncSchedulingServiceApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(PizzaCabinIncSchedulingServiceApplication.class, args);
	}
}
