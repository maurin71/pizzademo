package com.pizzacabin;

import com.pizzacabin.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class SchedulingService {

    final private static String SHORT_BREAK_COLOR = "#FF0000";
    final private static String LUNCH_COLOR = "#FFFF00";
    final private static String REST_SERVICE_URL = "http://pizzacabininc.azurewebsites.net/PizzaCabinInc.svc/schedule/";

    RestApiClient restApiClient;

    @Autowired
    public SchedulingService(RestApiClient restApiClient) {
        this.restApiClient = restApiClient;
    }

    public List<AvailableSlot> availableSlots(String date, ScheduleFormData scheduler) throws URISyntaxException {
        final ScheduleResponse scheduleResponse = restApiClient.fetch(REST_SERVICE_URL + date, ScheduleResponse.class);

        final List<Schedule> schedules = scheduleResponse.ScheduleResult.Schedules;

        Date firstAvailableSlot = firstAvailableTimeslot(schedules);
        Date lastAvailableSlot = lastAvailableTimeslot(schedules);
        long firstTimeslotInMilliseconds = firstAvailableSlot.getTime();
        long lastTimeslotInMilliseconds = lastAvailableSlot.getTime();
        List<AvailableSlot> availableSlots = new ArrayList<>();

        while (firstTimeslotInMilliseconds < lastTimeslotInMilliseconds) {
            long fifteenMinuteTimeslotInMilliseconds = 15 * 60 * 1000;
            long slotEndTime = firstTimeslotInMilliseconds + fifteenMinuteTimeslotInMilliseconds;
            int availCount = 0;
            List<Projection> projections = new ArrayList<>();
            for (Schedule schedule : schedules) {
                for (Projection projection: schedule.Projection) {
                    if (isTimeslotWithinProjection(firstTimeslotInMilliseconds, slotEndTime, projection) && isaBookableTimeslot(projection)) {
                        availCount = availCount + 1;
                        projection.name = schedule.Name;
                        projections.add(projection);
                    }
                }
            }
            if (hasFoundRequiredAmountOfSlots(scheduler, availCount)) {
                AvailableSlot as = new AvailableSlot();
                as.startTime =  writeDateString(firstTimeslotInMilliseconds);
                as.endTime =  writeDateString(slotEndTime);
                as.slotDescription = "Starting:"+ writeDateString(firstTimeslotInMilliseconds) +", Ending: "+ writeDateString(slotEndTime);
                as.available = availCount;
                as.projections = projections;
                availableSlots.add(as);
            }
            firstTimeslotInMilliseconds = slotEndTime;
        }
        return availableSlots;
    }


    private boolean hasFoundRequiredAmountOfSlots(ScheduleFormData scheduler, int availCount) {
        return availCount >= scheduler.getMandatoryNo();
    }

    private boolean isaBookableTimeslot(Projection projection) {
        return !SHORT_BREAK_COLOR.equals(projection.Color) && !LUNCH_COLOR.equals(projection.Color);
    }

    private boolean isTimeslotWithinProjection(long firstTimeslotInMilliseconds, long slotEndTime, Projection projection) {
        long projectionEnd = projection.Start.getTime() + projection.minutes * 60 * 1000;
        return projection.Start.getTime() <= firstTimeslotInMilliseconds && projectionEnd >= slotEndTime;
    }

    private Date lastAvailableTimeslot(List<Schedule> schedules) {
        return schedules.stream().map(s -> {
            if (!s.Projection.isEmpty()) {
                final Date startOfLastProjection = s.Projection.get(s.Projection.size() - 1).Start;
                final long startOfLastProjectionInMilliseconds = startOfLastProjection.getTime() + s.Projection.get(s.Projection.size() - 1).minutes * 60 * 1000;
                return new Date(startOfLastProjectionInMilliseconds);
            } else {
                return null;
            }
        }).filter(Objects::nonNull).max(Date::compareTo).get();
    }

    private Date firstAvailableTimeslot(List<Schedule> schedules) {
        return schedules.stream().map(s -> {
            if (!s.Projection.isEmpty()) {
                return s.Projection.get(0).Start;
            } else {
                return null;
            }
        }).filter(Objects::nonNull).min(Date::compareTo).get();
    }

    private String writeDateString(long millis) {
        LocalDateTime ldt = LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault());
        return ldt.toString();
    }
}
